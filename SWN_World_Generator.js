/*
    Created By:     Derek Lutz
    Creation Date:  May 14, 2024
    Description:    Generates a random world for Stars Without Number and creates a handout for the GM to use. Stars Without Number
        is an OSR Table Top RPG developed by Kevin Crawford. This script uses the tables in free version for generation.
    
    Free to use and modify, just leave the top attribution data. With your changes here.
    Check out my gitlab at https://gitlab.com/DerekLutz
    Modified By:
    Mod Date:
    Description:

*/

var swnGenerator = {
    isRestricted: true,
    getInput: function(msg){
        let args = msg.content.split(" ");
           
        if (msg.playerid.toLowerCase() === "api") {
            return;
        } 
        else{
            if((this.isRestricted && playerIsGM(msg.playerid)) || !this.isRestricted){
                switch(args[0].toLowerCase()){
                    case '.generateworld':
                    case '.gw':
                        for(i = 2; i < args.length; i++){
                            args[1] += " " + args[i];
                        }
                        return swnGenerator.generateWorld(args[1]);
                    case '.generatecreature':
                    case '.gc':
                        return swnGenerator.generateCreature();
                    case '.generatebeast':
                    case '.gb':
                        return swnGenerator.generateBeast();
                    case '.generatehuman':
                    case '.gh':
                        return swnGenerator.generateHuman();
                    case '.generatepoints':
                    case '.gp':
                        return swnGenerator.generatePoints();
                    case '.generaterobot':
                    case '.gr':
                        return swnGenerator.generateRobot();
                    case '.help':
                    case '.h':
                        sendChat('SWN Generator', "Generate World:\n .gw [name]\n .generateworld [name]\n"
                        + "Generate Creature:\n .gc \n .generatecreature \n"
                        + "Generate Beast:\n .gb \n .generatebeast \n"
                        + "Generate Human:\n .gh \n .generatehuman \n"
                        + "Generate Robot:\n .gr \n .generaterobot \n"
                        + "Generate Points:\n .gp \n .generatepoints \n"
                        + "Restrict:\n .r\n .restrict");
                        return;
                    case '.restrict':
                    case '.r':
                        if(this.isRestricted) sendChat('SWN Generator', 'Restricted Mode is off!');
                        else sendChat('SWN Generator', 'Restricted Mode is on!');
                        this.isRestricted = !this.isRestricted;
                        return;
                    default:
                        return;
                }
            }
            else{
                sendChat('SWN Generator', 'Access is restricted to GM only.');
                return;
            }
        }
    },
    rollDice: function(sides = 6){
        return Math.floor(Math.random() * sides) + 1;
    },
    createPlanetHandout: function(name, primaryTag, secondaryTag, temperature, biosphere, atomosphere, techLevel, population, creatures) {
    	let handout = createObj('handout',{
			name: name
		});
        publicNotes = `<h1>${name}</h1><br>`
        publicNotes += "<table>"
        //Row
        publicNotes += "<tr>"
        publicNotes += "<th> Atomosphere: </th>"
        publicNotes += `<td>${atomosphere}</td>`
        publicNotes += "</tr>"
        //Row
        publicNotes += "<tr>"
        publicNotes += "<th> Temperature: </th>"
        publicNotes += `<td>${temperature}</td>`
        publicNotes += "</tr>"
        //Row
        publicNotes += "<tr>"
        publicNotes += "<th> Biosphere: </th>"
        publicNotes += `<td>${biosphere}</td>`
        publicNotes += "</tr>"
        //Row
        publicNotes += "<tr>"
        publicNotes += "<th> Population: </th>"
        publicNotes += `<td>${population}</td>`
        publicNotes += "</tr>"

        // GM Notes section
        gmNotes = '';

        gmNotes += "<table>"
        //Row
        gmNotes += "<tr>"
        gmNotes += "<th> Tech Level: </th>"
        gmNotes += `<td>${techLevel}</td>`
        gmNotes += "</tr>"
        //Row
        gmNotes += "<tr>"
        gmNotes += "<th> Primary Tag: </th>"
        gmNotes += `<td>${primaryTag}</td>`
        gmNotes += "</tr>"
        //Row
        gmNotes += "<tr>"
        gmNotes += "<th> Secondary Tag: </th>"
        gmNotes += `<td>${secondaryTag}</td>`
        gmNotes += "</tr>"
        gmNotes += "</table>"
        gmNotes += "<BR>"


        //template:"Beast", type:type, style:{body:bodyPlan, limbs:limbNovelty, skin:skinNovelty}, weapon:mainWeapon, size:size, hd:HD, ac:AC, atk:Atk, dmg:Dmg, move:Move, ml:ML, skills:Skills, saves:Saves, behavior:behavior
        //template:"Human", type:type, hd:HD, ac:AC, atk:Atk, dmg:Dmg, move:Move, ml:ML, skills:Skills, saves:Saves
        //template:"Robot", type:type, hd:HD, ac:AC, atk:Atk, dmg:Dmg, move:Move, ml:ML, skills:Skills, saves:Saves, cost:Cost

        gmNotes += '<h3> Possible Fauna </h3>';
        if(creatures.length <= 0){
            gmNotes += "No noteworthy life forms detected.";
        } else{
            gmNotes += "<ul>"
            creatures.forEach(function(creature){
                let hp = 1;
                if(creature.hd !== "1 HP"){
                    hp = 0;
                    for(i = 0; i < creature.hd; i++){
                        hp += swnGenerator.rollDice(8);
                    }
                }

                gmNotes += `<li>${creature.type}</li>`;
                gmNotes += "<ul>";
                gmNotes += `<li> HD: ${creature.hd} (${hp} hp)</li>`;
                gmNotes += `<li> AC: ${creature.ac}</li>`;
                gmNotes += `<li> Attack: ${creature.atk}</li>`;
                gmNotes += `<li> Damage: ${creature.dmg}</li>`;
                gmNotes += `<li> Movement: ${creature.move}</li>`;
                gmNotes += `<li> Morale Level: ${creature.ml}</li>`;
                gmNotes += `<li> Skills: ${creature.skills}</li>`;
                gmNotes += `<li> Saves: ${creature.saves}</li>`;
                gmNotes += "<li> Description"
                gmNotes += "<ul>";
                gmNotes += `<li> Size: ${creature.style.size}</li>`;
                gmNotes += `<li> Body: ${creature.style.body}</li>`;
                gmNotes += `<li> Limbs: ${creature.style.limbs}</li>`;
                gmNotes += `<li> Skin: ${creature.style.skin}</li>`;
                gmNotes += `<li> Weapon: ${creature.weapon}</li>`; 
                gmNotes += `<li> Behavior: ${creature.style.behavior}</li>`;
                gmNotes += "</ul>";
                gmNotes += "</li>";
                gmNotes += "</ul>";
            });
            gmNotes += "</ul>";
        }

        handout.set('notes', publicNotes);
        handout.set('gmnotes', gmNotes);
        handout.set('inplayerjournals', "all");
        return handout;
	},
    registerEventHandlers: function(){
        console.log("Generator is listening!");
        on('chat:message', this.getInput);
    },
    generatePoints: function(){
        let pointsOfInterest = [];
        for(i = 0; i < this.rollDice(3); i++){
            pointsOfInterest[i] = this.getPointOfInterest();
        }

        let handout = createObj('handout',{
			name: "Points of Interest"
		});

        let gmNotes = "";
        let publicNotes = "";

        if(pointsOfInterest.length <= 0){
            gmNotes += "There is nothing noteworthy in this sector.";
        } 
        else{
            // Player descriptions
            publicNotes += "<ul>";
            pointsOfInterest.forEach(function(point){
                publicNotes += `<li> ${point.playerDesc} </li>`;
            });
            publicNotes += "</ul>";

            // GM Descriptions
            gmNotes += "<ul>";
            pointsOfInterest.forEach(function(point){
                gmNotes += `<li> ${point.fullDesc} </li>`;
            });
            gmNotes += "</ul>";
        }
        
        handout.set("notes", publicNotes);
        handout.set('gmnotes',gmNotes);

        sendChat("SWN Generator", `Some points been generated!`);
    },
    createID: function(){
        return this.rollDice(10000).toString().padStart(5,'0');
    },
    generateWorld: function(name = ""){
        

        if(name === ""){
            name = `New World ${this.createID()}`;
        }

        primaryTag = this.getTag();

        secondaryTag = this.getTag();
        while(secondaryTag === primaryTag){
            secondaryTag = this.getTag();
        }

        //Atomosphere
        switch (this.rollDice() + this.rollDice()){
            case 2:
                atomosphere = "Corrosive";
                break;
            case 3:
                atomosphere = "Inert";
                break;
            case 4:
                atomosphere = "Airless or thin";
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                atomosphere = "Breathable mix";
                break;
            case 10:
                atomosphere = "Thick";
                break;
            case 11:
                atomosphere = "Invasive";
                break;
            case 12:
                atomosphere = "Corrosive and Invasive";
                break;
            default:
                throw new RangeError("Atomosphere was out of range.");
        }

        //Population
        switch (this.rollDice() + this.rollDice()){
            case 2:
                population = "Failed Colony";
                break;
            case 3:
                population = "Outpost";
                break;
            case 4:
            case 5:
                population = "Fewer than a million inhabitants";
                break;
            case 6:
            case 7:
            case 8:
                population = "Several million inhabitants";
                break;
            case 9:
            case 10:
                population = "Hundreds of millions of inhabitants";
                break;
            case 11:
                population = "Billions of inhabitants";
                break;
            case 12:
                population = "Alien inhabitants";
                break;
            default:
                throw new RangeError("Population was out of range.");
        }

        //Temperature
        switch (this.rollDice() + this.rollDice()){
            case 2:
                temperature = "Frozen";
                break;
            case 3:
                temperature = "Cold";
                break;
            case 4:
            case 5:
                temperature = "Variable Cold";
                break;
            case 6:
            case 7:
            case 8:
                temperature = "Temperate";
                break;
            case 9:
            case 10:
                temperature = "Variable Warm";
                break;
            case 11:
                temperature = "Warm";
                break;
            case 12:
                temperature = "Burning";
                break;
            default:
                throw new RangeError("Temperature was out of range.");
        }

        //Tech Level
        switch (this.rollDice() + this.rollDice()){
            case 2:
                techLevel = "TL0: neolithic-level technology";
                break;
            case 3:
                techLevel = "TL1: medieval techonology";
                break;
            case 4:
            case 5:
                techLevel = "TL2: early Industrial Age tech";
                break;
            case 6:
            case 7:
            case 8:
                techLevel = "TL4: modern postech";
                break;
            case 9:
            case 10:
                techLevel = "TL3: tech like that of present-day Earth";
                break;
            case 11:
                techLevel = "TL4+: postech with specialites";
                break;
            case 12:
                techLevel = "TL5: pretech with surviving infrastructure";
                break;
            default:
                throw new RangeError("Tech Level was out of range.");
        }

        //Biosphere
        switch (this.rollDice() + this.rollDice()){
            case 2:
                biosphere = "Remnant Biosphere";
                break;
            case 3:
                biosphere = "Microbial life forms exist";
                break;
            case 4:
            case 5:
                biosphere = "No native biosphere";
                break;
            case 6:
            case 7:
            case 8:
                biosphere = "Human-miscible biosphere";
                break;
            case 9:
            case 10:
                biosphere = "Immiscible biosphere";
                break;
            case 11:
                biosphere = "Hybrid biosphere";
                break;
            case 12:
                biosphere = "Engineered biosphere";
                break;
            default:
                throw new RangeError("Biosphere was out of range.");
        }

        

        let creatures = [];
        for(i = 0; i < this.rollDice(6) + this.rollDice(4); i++){
            creatures[i] = this.generateBeast(false);
        }

        this.createPlanetHandout(name, primaryTag, secondaryTag, temperature, biosphere, atomosphere, techLevel, population, creatures);

        sendChat("SWN Generator", `${name} has been generated!`);
    },
    getPointOfInterest: function(){

        let place = "PLACE";
        let inhabitants = "INHABITANTS";
        let situation = "SITUATION";
    
    
        switch(this.rollDice(8)){
            case 1:
                place = " deep-space station"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "dangerously odd transhumans";
                        break;
                    case 2:
                        inhabitants = "freeze-dried ancient corpses";
                        break;
                    case 3:
                        inhabitants = "secretive military observers";
                        break;
                    case 4:
                        inhabitants = "an eccentric oligarch and minions";
                        break;
                    case 5:
                        inhabitants = "a deranged, but brillant scientist";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "systems breaking down";
                        break;
                    case 2:
                        situation = "a foreign sabotage attempt";
                        break;
                    case 3:
                        situation = "being a black market for the elite";
                        break;
                    case 4:
                        situation = "being a vault for dangerous pretech";
                        break;
                    case 5:
                        situation = "being a supply base for pirates";
                        break;
                }
            break;
            case 2:
                place = "n asteroid base"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "zelous religious sectarians";
                        break;
                    case 2:
                        inhabitants = "failed rebels from another world";
                        break;
                    case 3:
                        inhabitants = "wage-slave coporate miners";
                        break;
                    case 4:
                        inhabitants = "independent asteroid prospectors";
                        break;
                    case 5:
                        inhabitants = "pirates masquerading as otherwise";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "threatened lifesupport";
                        break;
                    case 2:
                        situation = "the base needing a new asteroid";
                        break;
                    case 3:
                        situation = "unearthing something nasty";
                        break;
                    case 4:
                        situation = "fighting another asteroid";
                        break;
                    case 5:
                        situation = "striking a priceless ore vein";
                        break;
                }
            break;
            case 3:
                place = " remote moon base"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "unlucky corporate researchers";
                        break;
                    case 2:
                        inhabitants = "a reclusive hermit genius";
                        break;
                    case 3:
                        inhabitants = "remnants of a failed colony";
                        break;
                    case 4:
                        inhabitants = "a military listening post";
                        break;
                    case 5:
                        inhabitants = "lonely overseers and robot miners";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "waking up something dark";
                        break;
                    case 2:
                        situation = "criminals trying to take over";
                        break;
                    case 3:
                        situation = "a moon plague breaking out";
                        break;
                    case 4:
                        situation = "a shortage of vital supplies";
                        break;
                    case 5:
                        situation = "being rich, but poorly protected";
                        break;
                }
            break;
            case 4:
                place = "n ancient orbital ruin"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "robots of dubious sentience";
                        break;
                    case 2:
                        inhabitants = "trigger-happy scavengers";
                        break;
                    case 3:
                        inhabitants = "government researchers";
                        break;
                    case 4:
                        inhabitants = "military quarantine enforcers";
                        break;
                    case 5:
                        inhabitants = "heirs of the original alien builders";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "trying to stop it awakening";
                        break;
                    case 2:
                        situation = "meddling with strange tech";
                        break;
                    case 3:
                        situation = "an impending tech calamity";
                        break;
                    case 4:
                        situation = "unearthing a terrible secret";
                        break;
                    case 5:
                        situation = "fighting outside interlopers";
                        break;
                }
            break;
            case 5:
                place = " research base"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "experiments that have gotten loose";
                        break;
                    case 2:
                        inhabitants = "scientists from a major local corp";
                        break;
                    case 3:
                        inhabitants = "black-ops governmental researchers";
                        break;
                    case 4:
                        inhabitants = "secret employees of a foreign power";
                        break;
                    case 5:
                        inhabitants = "aliens studying the locals";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "perilous research underway";
                        break;
                    case 2:
                        situation = "hideously immoral research";
                        break;
                    case 3:
                        situation = "being held hostage by outsiders";
                        break;
                    case 4:
                        situation = "science monsters running amok";
                        break;
                    case 5:
                        situation = "selling black market tech";
                        break;
                }
            break;
            case 6:
                place = "n asteroid belt"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "grizzled belter mine laborers";
                        break;
                    case 2:
                        inhabitants = "ancient automated guardian drones";
                        break;
                    case 3:
                        inhabitants = "suvivors of a destroyed asteroid base";
                        break;
                    case 4:
                        inhabitants = "pirates hiding out among the rocks";
                        break;
                    case 5:
                        inhabitants = "lonely military patrol staff";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "a ruptured rock releasing a peril";
                        break;
                    case 2:
                        situation = "foreign spy ships hiding nearby";
                        break;
                    case 3:
                        situation = "a gold rush for new minerals";
                        break;
                    case 4:
                        situation = "ancient alien ruins that dot the rocks";
                        break;
                    case 5:
                        situation = "war between rival rocks";
                        break;
                }
            break;
            case 7:
                place = " gas giant mine"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "miserable gas-miner slaves or serfs";
                        break;
                    case 2:
                        inhabitants = "strange robots and their overseers";
                        break;
                    case 3:
                        inhabitants = "scientists studying the alient life";
                        break;
                    case 4:
                        inhabitants = "scrappers in the ruined old mine";
                        break;
                    case 5:
                        inhabitants = "an impoverished separatists group";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "things emerging from below";
                        break;
                    case 2:
                        situation = "running out of vital supplies";
                        break;
                    case 3:
                        situation = "the workers revolting";
                        break;
                    case 4:
                        situation = "pirates secretly refueling there";
                        break;
                    case 5:
                        situation = "the discovery of alient remnants";
                        break;
                }
            break;
            case 8:
                place = " refueling station"
                switch(this.rollDice(5)){
                    case 1:
                        inhabitants = "a half-crazed hermit caretaker";
                        break;
                    case 2:
                        inhabitants = "sordid purveyors of decadent fun";
                        break;
                    case 3:
                        inhabitants = "extortionate corporate minions";
                        break;
                    case 4:
                        inhabitants = "religous missionaries and travelers";
                        break;
                    case 5:
                        inhabitants = "brainless automated vendors";
                        break;
                }
            
                switch(this.rollDice(5)){
                    case 1:
                        situation = "a ship in severe distress";
                        break;
                    case 2:
                        situation = "pirates taking over";
                        break;
                    case 3:
                        situation = "corrupt customs agents";
                        break;
                    case 4:
                        situation = "foreign saboteurs making their next move";
                        break;
                    case 5:
                        situation = "a deep-space alien signal";
                        break;
                }
            break;
        }
    
        return {fullDesc:`A${place} occupied by ${inhabitants} dealing with ${situation}.`, playerDesc:`A${place}`};
    },
    getTag: function() {
        let tag;
        switch(this.rollDice(100)) {
            case 1:
                tag = "Abandoned Colony";
                break;
            case 2:
                tag = "Alien Ruins";
                break;
            case 3:
                tag = "Altered Humanity";
                break;
            case 4:
                tag = "Anarchists";
                break;
            case 5:
                tag = "Anthropomorphs";
                break;
            case 6:
                tag = "Area 51";
                break;
            case 7:
                tag = "Badlands World";
                break;
            case 8:
                tag = "Battleground";
                break;
            case 9:
                tag = "Beastmasters";
                break;
            case 10:
                tag = "Bubble Cities";
                break;
            case 11:
                tag = "Cheap Life";
                break;
            case 12:
                tag = "Civil War";
                break;
            case 13:
                tag = "Cold War";
                break;
            case 14:
                tag = "Colonized Population";
                break;
            case 15:
                tag = "Cultural Power";
                break;
            case 16:
                tag = "Cybercommunists";
                break;
            case 17:
                tag = "Cyborgs";
                break;
            case 18:
                tag = "Cyclical Doom";
                break;
            case 19:
                tag = "Desert World";
                break;
            case 20:
                tag = "Doomed World";
                break;
            case 21:
                tag = "Dying Race";
                break;
            case 22:
                tag = "Eugenic Cult";
                break;
            case 23:
                tag = "Exchange Consulate";
                break;
            case 24:
                tag = "Fallen Hegemon";
                break;
            case 25:
                tag = "Feral World";
                break;
            case 26:
                tag = "Flying Cities";
                break;
            case 27:
                tag = "Forbidden Tech";
                break;
            case 28:
                tag = "Former Warriors";
                break;
            case 29:
                tag = "Freak Geology";
                break;
            case 30:
                tag = "Freak Weather";
                break;
            case 31:
                tag = "Friendly Foe";
                break;
            case 32:
                tag = "Gold Rush";
                break;
            case 33:
                tag = "Great Work";
                break;
            case 34:
                tag = "Hatred";
                break;
            case 35:
                tag = "Heavy Industry";
                break;
            case 36:
                tag = "Heavy Mining";
                break;
            case 37:
                tag = "Hivemind";
                break;
            case 38:
                tag = "Holy War";
                break;
            case 39:
                tag = "Hostile Biosphere";
                break;
            case 40:
                tag = "Hostile Space";
                break;
            case 41:
                tag = "Immortals";
                break;
            case 42:
                tag = "Local Specialty";
                break;
            case 43:
                tag = "Local Tech";
                break;
            case 44:
                tag = "Major Spaceyard";
                break;
            case 45:
                tag = "Mandarinate";
                break;
            case 46:
                tag = "Mandate Base";
                break;
            case 47:
                tag = "Maneaters";
                break;
            case 48:
                tag = "Megacorps";
                break;
            case 49:
                tag = "Mercenaries";
                break;
            case 50:
                tag = "Minimal Contact";
                break;
            case 51:
                tag = "Misandry/Misogyny";
                break;
            case 52:
                tag = "Night World";
                break;
            case 53:
                tag = "Nomads";
                break;
            case 54:
                tag = "Oceanic World";
                break;
            case 55:
                tag = "Out of Contact";
                break;
            case 56:
                tag = "Outpost World";
                break;
            case 57:
                tag = "Perimeter Agency";
                break;
            case 58:
                tag = "Pilgrimage Site";
                break;
            case 59:
                tag = "Pleasure World";
                break;
            case 60:
                tag = "Police State";
                break;
            case 61:
                tag = "Post-Scarcity";
                break;
            case 62:
                tag = "Preceptor Archive";
                break;
            case 63:
                tag = "Pretech Cultists";
                break;
            case 64:
                tag = "Primitive Aliens";
                break;
            case 65:
                tag = "Prison Planet";
                break;
            case 66:
                tag = "Psionics Academy";
                break;
            case 67:
                tag = "Psionics Fear";
                break;
            case 68:
                tag = "Psionics Worship";
                break;
            case 69:
                tag = "Quarantined World";
                break;
            case 70:
                tag = "Radioactive World";
                break;
            case 71:
                tag = "Refugees";
                break;
            case 72:
                tag = "Regional Hegemon";
                break;
            case 73:
                tag = "Restrictive Laws";
                break;
            case 74:
                tag = "Revanchists";
                break;
            case 75:
                tag = "Revolutionaries";
                break;
            case 76:
                tag = "Rigid Culture";
                break;
            case 77:
                tag = "Rising Hegemon";
                break;
            case 78:
                tag = "Ritual Combat";
                break;
            case 79:
                tag = "Robots";
                break;
            case 80:
                tag = "Seagoing Cities";
                break;
            case 81:
                tag = "Sealed Menace";
                break;
            case 82:
                tag = "Secret Masters";
                break;
            case 83:
                tag = "Sectarians";
                break;
            case 84:
                tag = "Seismic Instability";
                break;
            case 85:
                tag = "Shackled World";
                break;
            case 86:
                tag = "Societal Despair";
                break;
            case 87:
                tag = "Sole Supplier";
                break;
            case 88:
                tag = "Taboo Treasure";
                break;
            case 89:
                tag = "Terraform Failure";
                break;
            case 90:
                tag = "Theocracy";
                break;
            case 91:
                tag = "Tomb World";
                break;
            case 92:
                tag = "Trade Hub";
                break;
            case 93:
                tag = "Tyranny";
                break;
            case 94:
                tag = "Unbraked AI";
                break;
            case 95:
                tag = "Urbanized Surface";
                break;
            case 96:
                tag = "Utopia";
                break;
            case 97:
                tag = "Warlords";
                break;
            case 98:
                tag = "Xenophiles";
                break;
            case 99:
                tag = "Xenophobes";
                break;
            case 100:
                tag = "Zombies";
                break;
            default:
                throw new RangeError("Tag was out of range.");
        }
        return tag;
    }, 
    generateBeast: function(makeHandout = true){
        let type, bodyPlan, limbNovelty, skinNovelty, mainWeapon, size, HD, AC, Atk, Dmg, Move, ML, Skills, Saves, behavior;
        let niche;
        let isSmall = false;
        let isMedium = false;
        let isLarge = false;

        switch (this.rollDice() + this.rollDice()) {
            case 2:
            case 3:
            case 4:
                niche = "Prey";
                type = "Large Aggressive Prey Animal";
                HD = 5;
                AC = 13;
                Atk = "+4";
                Dmg = "1d10";
                Move = "15m";
                ML = 8;
                Skills = "+1";
                Saves = "12+";
                isLarge = true;
                break;
            case 5:
            case 6:
                niche = this.rollDice(2) == 1 ? "Prey":"Scavenger";

                type = "Small Vicious Beast";
                HD = "1 HP";
                AC = 14;
                Atk = "+1";
                Dmg = "1d2";
                Move = "10m";
                ML = 7;
                Skills = "+1";
                Saves = "15+";
                isSmall = true;
                break;
            case 7:
                niche = this.rollDice(2) == 1 ? "Prey":"Predator";
                niche = this.rollDice(2) == 1 ? niche:"Scavenger";

                type = "Small Pack Hunter";
                HD = 1;
                AC = 13;
                Atk = "+1";
                Dmg = "1d4";
                Move = "15m";
                ML = 8;
                Skills = "+1";
                Saves = "15+";
                isSmall = true;
                break;
            case 8:
                niche = this.rollDice(2) == 1 ? "Scavenger":"Predator";

                type = "Large Pack Hunter";
                HD = 2;
                AC = 14;
                Atk = "+2";
                Dmg = "1d6";
                Move = "15m";
                ML = 9;
                Skills = "+1";
                Saves = "14+";
                isMedium = true;
                break;
            
            case 9:
                niche = this.rollDice(3) == 1 ? "Scavenger": "Predator";

                type = "Lesser Lone Predator";
                HD = 3;
                AC = 14;
                Atk = "+4 x 2";
                Dmg = "1d8 each";
                Move = "15m";
                ML = 8;
                Skills = "+2";
                Saves = "14+";
                isMedium = true;
                break;
            case 10:
                niche = "Predator";

                type = "Greater Lone Predator";
                HD = 5;
                AC = 15;
                Atk = "+6 x 2";
                Dmg = "1d10 each";
                Move = "10m";
                ML = 9;
                Skills = "+2";
                Saves = "12+";
                isLarge = true;
                break;
            case 11:
                niche = "Predator";

                type = "Terrifying Apex Predator";
                HD = 8;
                AC = 16;
                Atk = "+8 x 2";
                Dmg = "1d10 each";
                Move = "20m";
                ML = 9;
                Skills = "+2";
                Saves = "11+";
                isLarge = true;
                break;
            case 12:
                niche = "Predator";

                type = "Engineered Murder Beast";
                HD = 10;
                AC = 18;
                Atk = "+10 x 4";
                Dmg = "1d10 each";
                Move = "20m";
                ML = 11;
                Skills = "+3";
                Saves = "10+";
                break;
            default:
                throw new RangeError("Beast out of range.");
        }

        switch (niche) {
            case "Predator":
                switch (this.rollDice(8)) {
                    case 1:
                        behavior = "Hunts in kin-group packs";
                        break;
                    case 2:
                        behavior = "Favors ambush attacks";
                        break;
                    case 3:
                        behavior = "Cripples prey and waits for death";
                        break;
                    case 4:
                        behavior = "Pack supports alpha-beast attack";
                        break;
                    case 5:
                        behavior = "Lures or drives prey into danger";
                        break;
                    case 6:
                        behavior = "Hunts as a lone, powerful hunter";
                        break;
                    case 7:
                        behavior = "Only is predator at certain times";
                        break;
                    case 8:
                        behavior = "Mindlessly attacks humans";
                        break;
                    default:
                        behavior = "Unknown behavior";
                }
                break;
            case "Prey":
                switch (this.rollDice(8)) {
                    case 1:
                        behavior = "Moves in vigilant herds";
                        break;
                    case 2:
                        behavior = "Exists in small family groups";
                        break;
                    case 3:
                        behavior = "They all team up on a single foe";
                        break;
                    case 4:
                        behavior = "They go berserk when near death";
                        break;
                    case 5:
                        behavior = "They're violent in certain seasons";
                        break;
                    case 6:
                        behavior = "They're vicious if threatened";
                        break;
                    case 7:
                        behavior = "Symbiotic creature protects them";
                        break;
                    case 8:
                        behavior = "Breeds at tremendous rates";
                        break;
                    default:
                        behavior = "Unknown behavior";
                }
                break;
            case "Scavenger":
                switch (this.rollDice(8)) {
                    case 1:
                        behavior = "Never attacks unwounded prey";
                        break;
                    case 2:
                        behavior = "Uses other beasts as harriers";
                        break;
                    case 3:
                        behavior = "Always flees if significantly hurt";
                        break;
                    case 4:
                        behavior = "Poisons prey, waits for it to die";
                        break;
                    case 5:
                        behavior = "Disguises itself as its prey";
                        break;
                    case 6:
                        behavior = "Remarkably stealthy";
                        break;
                    case 7:
                        behavior = "Summons predators to weak prey";
                        break;
                    case 8:
                        behavior = "Steals prey from weaker predator";
                        break;
                    default:
                        behavior = "Unknown behavior";
                }
                break;
            default:
                throw new RangeError("Behavior is out of range");
        }


        switch (this.rollDice()) {
            case 1:
                bodyPlan = "Humanoid";
                break;
            case 2:
                bodyPlan = "Quadruped";
                break;
            case 3:
                bodyPlan = "Many-legged";
                break;
            case 4:
                bodyPlan = "Bulbous";
                break;
            case 5:
                bodyPlan = "Amorphous";
                break;
            case 6:
                let die1 = this.rollDice(5);
                let die2 = this.rollDice(5);

                while (die1 == die2){
                    die2 = this.rollDice(5);
                }

                bodyPlan = "";

                switch(die1){
                    case 1:
                        bodyPlan += "Humanoid, ";
                        break;
                    case 2:
                        bodyPlan += "Quadruped, ";
                        break;
                    case 3:
                        bodyPlan += "Many-legged, ";
                        break;
                    case 4:
                        bodyPlan += "Bulbous, ";
                        break;
                    case 5:
                        bodyPlan += "Amorphous, ";
                        break;
                }

                switch(die2){
                    case 1:
                        bodyPlan += "Humanoid";
                        break;
                    case 2:
                        bodyPlan += "Quadruped";
                        break;
                    case 3:
                        bodyPlan += "Many-legged";
                        break;
                    case 4:
                        bodyPlan += "Bulbous";
                        break;
                    case 5:
                        bodyPlan += "Amorphous";
                        break;
                }
                break;
            default:
                throw new RangeError("Body Plan is out of range");
        }
        
        switch (this.rollDice()) {
            case 1:
                limbNovelty = "Wings";
                break;
            case 2:
                limbNovelty = "Many joints";
                break;
            case 3:
                limbNovelty = "Tentacles";
                break;
            case 4:
                limbNovelty = "Opposable thumbs";
                break;
            case 5:
                limbNovelty = "Retractable";
                break;
            case 6:
                limbNovelty = "Varying sizes";
                break;
            default:
                throw new RangeError("Limb Novelty is out of range");
        }
        
        switch (this.rollDice()) {
            case 1:
                skinNovelty = "Hard shell";
                break;
            case 2:
                skinNovelty = "Exoskeleton";
                break;
            case 3:
                skinNovelty = "Odd texture";
                break;
            case 4:
                skinNovelty = "Molts regularly";
                break;
            case 5:
                skinNovelty = "Harmful to touch";
                break;
            case 6:
                skinNovelty = "Wet or slimy";
                break;
            default:
                throw new RangeError("Skin Novelty is out of range");
        }
        
        switch (this.rollDice()) {
            case 1:
                mainWeapon = "Teeth or mandibles";
                break;
            case 2:
                mainWeapon = "Claws";
                break;
            case 3:
                mainWeapon = "Poison";
                break;
            case 4:
                mainWeapon = "Harmful discharge";
                break;
            case 5:
                mainWeapon = "Pincers";
                break;
            case 6:
                mainWeapon = "Horns";
                break;
            default:
                throw new RangeError("Main Weapon is out of range");
        }

        let sizeRoll = this.rollDice();

        if (isLarge) sizeRoll = this.rollDice(2) + 4;
        else if (isMedium) sizeRoll = this.rollDice(3) + 1
        else if (isSmall) sizeRoll = this.rollDice(2);

        switch (sizeRoll) {
            case 1:
                size = "Cat-sized";
                break;
            case 2:
                size = "Wolf-sized";
                break;
            case 3:
                size = "Calf-sized";
                break;
            case 4:
                size = "Bull-sized";
                break;
            case 5:
                size = "Hippo-sized";
                break;
            case 6:
                size = "Elephant-sized";
                break;
            default:
                throw new RangeError("Size is out of range");
        }

        if(mainWeapon === "Poison"){
            let effect, onset, duration;
            
            mainWeapon = `<b>${mainWeapon}</b>: `
            switch (this.rollDice()) {
                case 1:
                    effect = "Death";
                    break;
                case 2:
                    effect = "Paralysis";
                    break;
                case 3:
                    effect = `<b>${this.rollDice(4)}</b> dmg per onset interval`;
                    break;
                case 4:
                    effect = "Convulsions";
                    break;
                case 5:
                    effect = "Blindness";
                    break;
                case 6:
                    effect = "Hallucinations";
                    break;
                default:
                    throw new RangeError("Effect is out of range");
            }
            
            switch (this.rollDice()) {
                case 1:
                    onset = "an Instant";
                    break;
                case 2:
                    onset = "<b>1</b> round";
                    break;
                case 3:
                    onset = `<b>${this.rollDice()}</b> rounds`;
                    break;
                case 4:
                    onset = "<b>1</b> minute";
                    break;
                case 5:
                    onset = `${this.rollDice()} minutes`;
                    break;
                case 6:
                    onset = "<b>1</b> hour";
                    break;
                default:
                    throw new RangeError("Onset is out of range.");
            }
            
            switch (this.rollDice()) {
                case 1:
                    duration = `<b>${this.rollDice()}</b> rounds`;
                    break;
                case 2:
                    duration = "<b>1</b> minute";
                    break;
                case 3:
                    duration = "<b>10</b> minutes";
                    break;
                case 4:
                    duration = "<b>1</b> hour";
                    break;
                case 5:
                    duration = `<b>${this.rollDice()}</b> hours`;
                    break;
                case 6:
                    duration = `<b>${this.rollDice()}</b> days`;
                    break;
                default:
                    throw new RangeError("Duration is out of range.")
            }

            mainWeapon += `Causes ${effect} with an onset of ${onset}.`;
            if(effect !== "Death"){
                mainWeapon += ` The poison lasts for ${duration}.`;
            }
        } else if(mainWeapon === "Harmful discharge"){

            let discharge;
            switch (this.rollDice(8)) {
                case 1:
                    discharge = `<b>${this.rollDice()}</b> rounds`;
                    break;
                case 2:
                    discharge = "Acidic spew doing its damage on a hit";
                    break;
                case 3:
                    discharge = "Super-heated or super-chilled spew";
                    break;
                case 4:
                    discharge = "Sonic drill or other disabling noise";
                    break;
                case 5:
                    discharge = `Natural laser or plasma discharge`;
                    break;
                case 6:
                    discharge = `Nauseating stench or disabling chemical`;
                    break;
                case 7:
                    discharge = `Equipment-melting corrosive`;
                    break;
                case 8:
                    discharge = `Explosive pellets or chemical catalysts`;
                    break;
                default:
                    throw new RangeError("discharge is out of range.")
            }
            mainWeapon = `<b>${mainWeapon}: ${discharge}.</b>`

        }

        if(makeHandout){
            let beastID = this.createID();
            let handout = createObj('handout',{
                name: `Unnamed Beast ${beastID}`
            });

            let gmNotes = "", publicNotes = "";
            let hp = 1;
            if(HD !== "1 HP"){
                hp = 0;
                for(i = 0; i < HD; i++){
                    hp += swnGenerator.rollDice(8);
                }
            }
            gmNotes += "<h3> Stats </h3>";
            gmNotes += "<ul>";
            gmNotes += `<li> HD: ${HD} (${hp} hp)</li>`;
            gmNotes += `<li> AC: ${AC}</li>`;
            gmNotes += `<li> Attack: ${Atk}</li>`;
            gmNotes += `<li> Damage: ${Dmg}</li>`;
            gmNotes += `<li> Movement: ${Move}</li>`;
            gmNotes += `<li> Morale Level: ${ML}</li>`;
            gmNotes += `<li> Skills: ${Skills}</li>`;
            gmNotes += `<li> Saves: ${Saves}</li>`;
            gmNotes += `<li> Weapon: ${mainWeapon}</li>`; 
            gmNotes += `<li> Behavior: ${behavior}</li>`;
            gmNotes += "</ul>";

            publicNotes += "<h3>Description</h3>"
            publicNotes += `<h5>${type} ${beastID}</h5>`;
            publicNotes += "<ul>";
            publicNotes += `<li> Size: ${size}</li>`;
            publicNotes += `<li> Body: ${bodyPlan}</li>`;
            publicNotes += `<li> Limbs: ${limbNovelty}</li>`;
            publicNotes += `<li> Skin: ${skinNovelty}</li>`;
            publicNotes += "</ul>";


            handout.set("notes", publicNotes)
            handout.set('gmnotes',gmNotes);
            return handout;
        }

        return {template:"Beast", type:type, style:{body:bodyPlan, limbs:limbNovelty, skin:skinNovelty, behavior:behavior, size:size,}, weapon:mainWeapon,  hd:HD, ac:AC, atk:Atk, dmg:Dmg, move:Move, ml:ML, skills:Skills, saves:Saves};
    },
    generateHuman: function(makeHandout = true){
        let type, HD, AC, Atk, Dmg, Move, ML, Skills, Saves;

        //TO DO: Add weapons
        switch (this.rollDice(5)) {
            case 1:
                type = "Peaceful Human";
                HD = 1;
                AC = 10;
                Atk = "+0";
                Dmg = "Unarmed";
                Move = "10m";
                ML = 6;
                Skills = "+1";
                Saves = "15+";
                break;
            case 2:
                type = "Martial Human";
                HD = 1;
                AC = 10;
                Atk = "+1";
                Dmg = "By weapon";
                Move = "10m";
                ML = 8;
                Skills = "+1";
                Saves = "15+";
                break;
            case 3:
                type = "Veteran Fighter";
                HD = 2;
                AC = 14;
                Atk = "+2";
                Dmg = "By weapon +1";
                Move = "10m";
                ML = 9;
                Skills = "+1";
                Saves = "14+";
                break;
            case 4:
                type = "Elite Fighter";
                HD = 3;
                AC = "16 (combat)";
                Atk = "+4";
                Dmg = "By weapon +1";
                Move = "10m";
                ML = 10;
                Skills = "+2";
                Saves = "14+";
                break;
            case 5:
                type = "Heroic Fighter";
                HD = 6;
                AC = "16 (combat)";
                Atk = "+8";
                Dmg = "By weapon +3";
                Move = "10m";
                ML = 11;
                Skills = "+3";
                Saves = "12+";
                break;
            default:
                throw new RangeError("Human NPC out of range.")
        }

        if(makeHandout){
            let handout = createObj('handout',{
                name: "Unnamed Human"
            });

            let gmNotes = "", publicNotes = "";
            let hp = 1;
                if(HD !== "1 HP"){
                    hp = 0;
                    for(i = 0; i < HD; i++){
                        hp += swnGenerator.rollDice(8);
                    }
                }
            gmNotes += "<h3> Stats </h3>";
            gmNotes += "<ul>";
            gmNotes += `<li> HD: ${HD} (${hp} hp)</li>`;
            gmNotes += `<li> AC: ${AC}</li>`;
            gmNotes += `<li> Attack: ${Atk}</li>`;
            gmNotes += `<li> Damage: ${Dmg}</li>`;
            gmNotes += `<li> Movement: ${Move}</li>`;
            gmNotes += `<li> Morale Level: ${ML}</li>`;
            gmNotes += `<li> Skills: ${Skills}</li>`;
            gmNotes += `<li> Saves: ${Saves}</li>`;
            gmNotes += "</ul>";

            publicNotes += "<h3>Description</h3>"
            publicNotes += `<h5>${type}</h5>`;


            handout.set("notes", publicNotes)
            handout.set('gmnotes',gmNotes);
            return handout;
        }

        return {template:"Human", type:type, hd:HD, ac:AC, atk:Atk, dmg:Dmg, move:Move, ml:ML, skills:Skills, saves:Saves};

    },
    generateRobot: function(makeHandout = true){
        let type, HD, AC, Atk, Dmg, Move, ML, Skills, Saves, Cost;

        switch (this.rollDice(7)) {
            case 1:
                type = "Janitor Bot";
                HD = 1;
                AC = 14;
                Atk = "N/A";
                Dmg = "N/A";
                Move = "5m";
                ML = 8;
                Skills = "+1";
                Saves = "15+";
                Cost = 1000;
                break;
            case 2:
                type = "Civilian Security Bot";
                HD = 1;
                AC = 15;
                Atk = "+1";
                Dmg = "1d8 stun";
                Move = "10m";
                ML = 12;
                Skills = "+1";
                Saves = "15+";
                Cost = 5000;
                break;
            case 3:
                type = "Repair Bot";
                HD = 1;
                AC = 14;
                Atk = "+0";
                Dmg = "1d6 tool";
                Move = "10m";
                ML = 8;
                Skills = "+1";
                Saves = "15+";
                Cost = 5000;
                break;
            case 4:
                type = "Industrial Work Bot";
                HD = 2;
                AC = 15;
                Atk = "+0";
                Dmg = "1d10 crush";
                Move = "5m";
                ML = 8;
                Skills = "+1";
                Saves = "14+";
                Cost = 2000;
                break;
            case 5:
                type = "Companion Bot";
                HD = 1;
                AC = 12;
                Atk = "+0";
                Dmg = "1d2 unarmed";
                Move = "10m";
                ML = 6;
                Skills = "+1";
                Saves = "15+";
                Cost = 2500;
                break;
            case 6:
                type = "Soldier Bot";
                HD = 2;
                AC = 16;
                Atk = "+1";
                Dmg = "By weapon";
                Move = "10m";
                ML = 10;
                Skills = "+1";
                Saves = "14+";
                Cost = 10000;
                break;
            case 7:
                type = "Heavy Warbot";
                HD = 6;
                AC = 18;
                Atk = "+8 x 2";
                Dmg = "2d8 plasma";
                Move = "15m";
                ML = 10;
                Skills = "+2";
                Saves = "12+";
                Cost = 50000;
                break;
            default:
                throw new RangeError("Robot out of range.");
        }

        if(makeHandout){
            let handout = createObj('handout',{
                name: "Unnamed Robot"
            });

            let gmNotes = "", publicNotes = "";
            let hp = 1;
                if(HD !== "1 HP"){
                    hp = 0;
                    for(i = 0; i < HD; i++){
                        hp += swnGenerator.rollDice(8);
                    }
                }
            gmNotes += "<h3> Stats </h3>";
            gmNotes += "<ul>";
            gmNotes += `<li> HD: ${HD} (${hp} hp)</li>`;
            gmNotes += `<li> AC: ${AC}</li>`;
            gmNotes += `<li> Attack: ${Atk}</li>`;
            gmNotes += `<li> Damage: ${Dmg}</li>`;
            gmNotes += `<li> Movement: ${Move}</li>`;
            gmNotes += `<li> Morale Level: ${ML}</li>`;
            gmNotes += `<li> Skills: ${Skills}</li>`;
            gmNotes += `<li> Saves: ${Saves}</li>`;
            gmNotes += "</ul>";

            publicNotes += "<h3>Description</h3>"
            publicNotes += `<h5>${type}</h5>`;
            publicNotes += "<ul>";
            publicNotes += `<li>Cost: ${Cost}</li>`
            publicNotes += "</ul>";

            handout.set("notes", publicNotes)
            handout.set('gmnotes',gmNotes);
            return handout;
        }


        return {template:"Robot", type:type, hd:HD, ac:AC, atk:Atk, dmg:Dmg, move:Move, ml:ML, skills:Skills, saves:Saves, cost:Cost};   
    },
    generateCreature: function(makeHandout = true){
        switch(this.rollDice() + this.rollDice()){
            case 2:
            case 3:
            case 11:
            case 12:
                return this.generateRobot(makeHandout);
            case 4:
            case 5:
            case 9:
            case 10:
                return this.generateHuman(makeHandout);
            case 6:
            case 7:
            case 8:
                return this.generateBeast(makeHandout);
            default:
                throw new RangeError("Creature out of range");      
        }
    }
}

on("ready", () => {
    "use strict";
    swnGenerator.registerEventHandlers();
    sendChat('SWN Generator', 'Derek\'s Generator is ready!\n');
    sendChat('SWN Generator', 'To get started type .h or .help');
});

