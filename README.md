 # SWN_Generator

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Name
Stars Without Number Generator

## Description
Generates world, creatures, and points of interest for a gm running Stars Without Number.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Roadmap
Adding weapons to humans. Adding Star Systems.

## Contributing
Contributions are accepted.

## Authors and acknowledgment
Stars Without Number is a table top rpg created by Kevin Crawford. I make no claim to its content.

Created: By Derek Lutz
Creation Date: May 14, 2024

## Project status
Active
